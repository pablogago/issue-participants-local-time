# Issue Participants Local Time

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE)

### Description

The app adds the *Open Participants Local Time* button using an 
[issue glance](https://developer.atlassian.com/platform/forge/manifest-reference/#jira-issue-glance)
which displays alongside fields such as Assignee and Labels.

![Issue glance showing a button with text "Open Participants Local Time"](docs/images/forge-glance.png)

Clicking on the issue glance button opens a panel that displays the issue's participants local time and whether they're online at the moment or not.

![Glance panel showing a Jira issue with participants local time from the Forge app](docs/images/forge-glance-panel.png)

## Installation

If this is your first time using Forge, the
[getting started](https://developer.atlassian.com/platform/forge/set-up-forge/)
guide will help you install the prerequisites.

If you already have a Forge environment set up, you can deploy this example straight
away. Visit the [example apps](https://developer.atlassian.com/platform/forge/example-apps/)
page for installation steps.

## Documentation

The app's [manifest.yml](./manifest.yml) contains two modules:

1. A [jira:issueGlance module](https://developer.atlassian.com/platform/forge/manifest-reference/#jira-issue-glance)
that specifies the metadata displayed to the user using a [Glance](https://developer.atlassian.com/cloud/jira/platform/modules/issue-glance/) in the Jira Issue View. The `jira:issueGlance` uses the following fields:
  
    * `title` displayed above glance button.
    * `label` displayed on glance button.
    * `status` lozenge displaying ">>".

1. A corresponding [function module](https://developer.atlassian.com/platform/forge/manifest-reference/#function)
that implements the issue glance logic.

The function logic is implemented in two files:

* [src/index.jsx](./src/index.jsx): Contains the main logic and UI elements of the app.
* [src/helpers.js](./src/helpers.js): Contains helper functions.

The app's UI is implemented using these features:

- [`IssueGlance`](https://developer.atlassian.com/platform/forge/ui-components/issue-glance) component
- [`Avatar`](https://developer.atlassian.com/platform/forge/ui-components/avatar) component
- [`AvatarStack`](https://developer.atlassian.com/platform/forge/ui-components/avatar-stack) component
- [`Button`](https://developer.atlassian.com/platform/forge/ui-components/button) component
- [`Text`](https://developer.atlassian.com/platform/forge/ui-components/text) component
- [`ModalDialog`](https://developer.atlassian.com/platform/forge/ui-components/modal-dialog) component
- [`Form`](https://developer.atlassian.com/platform/forge/ui-components/form) component
- [`Lozenge`](https://developer.atlassian.com/platform/forge/ui-components/lozenge) component
- [`Table`](https://developer.atlassian.com/platform/forge/ui-components/table) component
- [`useState`](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#usestate)
- [`useProductContext`](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#useproductcontext)

## Contributions

Contributions to Issue participants local time are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## License

Copyright (c) 2020 Pablo García Abásolo.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
