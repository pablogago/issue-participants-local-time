import api from "@forge/api";

export const getDataFromJira = async url => {
  try {
    const response = await api.asUser().requestJira(url);
    return await response.json();
  } catch (error) {
    console.log("getDataFromJira error: ", error);
    throw error;
  }
};

const composeIssueUrl = (issueId) => `/rest/api/2/issue/${issueId}`;

export const getUsersTimezones = async (issueId) => {
  const issueUrl = composeIssueUrl(issueId);
  const issue = await getDataFromJira(issueUrl);
  const fields = ['creator', 'assignee', 'reporter'];
  const timezones = [];
  for (let field of fields) {
    const user = issue.fields[field];
    const found = timezones.find(t => t.accountId === user.accountId);
    if (!found && user.accountType !== 'app') {
      timezones.push({
        accountId: user.accountId,
        timezone: new Date().toLocaleString("es-ES", {
          timeZone: user.timeZone,
          hour12: false,
          timeStyle: 'short'
        }),
        name: user.displayName,
        online: user.active
      });
    }
  }
  return timezones;
}