import ForgeUI, { render, Table, Head, Row, Cell, Text, useProductContext, useState } from '@forge/ui';
import { getUsersTimezones } from './helpers';

const UsersTimezones = ({ usersTimezones }) => {
  return (
    <Table>
      <Head>
        <Cell>
          <Text content="User" />
        </Cell>
        <Cell>
          <Text content="Local time" />
        </Cell>
        <Cell>
          <Text content="Online" />
        </Cell>
      </Head>
      {usersTimezones.map(user => {
        const { name, timezone, online } = user;
        return (
          <Row>
            <Cell>
              <Text content={name} />
            </Cell>
            <Cell>
              <Text content={timezone} />
            </Cell>
            <Cell>
              <Text content={online ? '🟢' : '🔴'} />
            </Cell>
          </Row>
        )
      })}
    </Table>
  );
};

const App = () => {
  const { platformContext } = useProductContext();
  const [usersTimezones] = useState(async () => await getUsersTimezones(platformContext.issueId));
  return (
    <UsersTimezones usersTimezones={usersTimezones}/>    
  );
};

export const run = render(<App/>);